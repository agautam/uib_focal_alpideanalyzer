#ifndef WRITENOISYPIXELBITMAP
#define WRITENOISYPIXELBITMAP

#include <vector>
#include "readALPIDETree.h"

int writeNoisyPixelBitmap(int id, std::vector<noisyPixel> noisyPixels);
int createPixelMaskMap(std::vector<noisyPixel> noisyPixels);
#endif
