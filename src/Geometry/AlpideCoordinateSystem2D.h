#include <vector>

#include "AlpideMatrix.h"

#ifndef ALPIDE_POSITION
#define ALPIDE_POSITION

#define ALPIDE_N_COLS 1024 // x-direction
#define ALPIDE_N_ROWS 512  // y-direction
#define ALPIDE_COL_MAX 1023
#define ALPIDE_ROW_MAX 511

class AlpideCoordinateSystem2D : AlpideMatrix {

    private:
        int px_x_offset;
        int px_y_offset;
        bool x_axis_inverted;
        bool y_axis_inverted;

    public:
        AlpideCoordinateSystem2D();
        AlpideCoordinateSystem2D(int _offx, int _offy, bool _invx, bool _invy);
        void HitLocal2Global(std::pair<unsigned short, unsigned short> &localpx, std::pair<unsigned short, unsigned short> &globalpx);
        void HitGlobal2Local(std::pair<unsigned short, unsigned short> &globalpx, std::pair<unsigned short, unsigned short> &localpx);
        int GetX0();
        int GetY0();

};

#endif