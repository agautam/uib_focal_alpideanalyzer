#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <fstream>
#include <unistd.h>
#include <vector>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <map>

using namespace std;

#define LENGTH_DATA 10
#define LENGTH_ZERO 6

#define resx 3072
#define resy 3072

#define SIZE (resx*resy*sizeof(uint32_t))

bool verbose = false;

vector<int> alpideIDs;

int alpideBX;
int alpideID;
int alpideX;
int alpideY;
unsigned int triggerORBIT;
unsigned int triggerBX;
unsigned int triggerBX_delta;
long unsigned int triggerTime;

unsigned int feeIDLayer;
unsigned int feeIDGBTLink;
unsigned int feeIDStaveNumber;

struct ru_info {
    string shm_name;
    bool flipped;
};

map<int, ru_info> ru_infos;

void nextLine();

unsigned char rawdata[LENGTH_DATA];
unsigned char zero[LENGTH_ZERO];

ifstream infile;
unsigned char allAlpides[3000];
unsigned char allIDs[9];
long int nTriggers;

unsigned char alpide[3000] = {};

int getAlpideY(uint16_t address);
int getAlpideX(uint8_t region, uint8_t encoder, uint16_t address);
int chip_to_world_x(int ru, int chipid, int x);
int chip_to_world_y(int ru, int chipid, int y);

unsigned int lengthAlpideData = 0;
void readALPIDEData(unsigned int length);
void readALPIDEData();


// some control stuff
bool its = false;
bool rdh = false;
bool trigger = false;
int nActiveLanes = 0;
unsigned int alpidecounter = 0;

// Analyze the headers and trailers
bool isITSHeader(char a) { return (unsigned char) a == 0xe0; }
bool isTriggerHeader(char a) { return (unsigned char) a == 0xe8; }
bool isTriggerTrailer(char a) { return (unsigned char) a == 0xf0; }
bool isRDHHeader(char version, char size) { return ((unsigned char) version == 0x06) && ((unsigned char) size == 0x40); }
bool isALPIDEEmptyFrame(unsigned char a) { return (a & 0xf0) == 0xe0; }
bool isALPIDEHeader(unsigned char a) { return (a & 0xf0) == 0xa0; }

map<string, uint32_t *> pixels;

void drawHit(int ru, int x, int y, uint32_t color)
{
    if (!ru_infos.count(ru)) {
        if (verbose) printf("Found unconfigured readout unit.");
        return;
    }
    if (x > 3071 || y > 3071) {
        if (verbose) printf("Pixel out of bounds: %d, %d", x, y);
        return;
    }
    uint32_t *memory_thingy = pixels[ru_infos[ru].shm_name];
    memory_thingy[y * resx + x] = color;

    if (x > 0) memory_thingy[y * resx + x - 1] = color;
    if (x < 3071) memory_thingy[y * resx + x + 1] = color;
    if (y > 0) memory_thingy[(y - 1) * resx + x] = color;
    if (y < 3071) memory_thingy[(y + 1) * resx + x] = color;

    if (x > 0 && y > 0) memory_thingy[(y - 1) * resx + x - 1] = color;
    if (x < 3071 && y < 3071) memory_thingy[(y + 1) * resx + x + 1] = color;
    if (x > 0 && y < 3071) memory_thingy[(y + 1) * resx + x - 1] = color;
    if (x < 3071 && y > 0) memory_thingy[(y - 1) * resx + x + 1] = color;

    if (x > 1) memory_thingy[y * resx + x - 2] = color;
    if (x < 3070) memory_thingy[y * resx + x + 2] = color;
    if (y > 1) memory_thingy[(y - 2) * resx + x] = color;
    if (y < 3070) memory_thingy[(y + 2) * resx + x] = color;
}

int main(int argc, char *argv[])
{
    string filename;
    long eof_offset = 0; // By default, we start at current end of file

    // Argument parsing
    string current_arg;
    for (int i = 0; i < argc; ++i) {
        current_arg = argv[i];
        if (current_arg == "--ru") {
            if (argc - i < 4) {
                printf("Too few arguments for RU (--ru)");
                return 1;
            }

            int ru = atoi(argv[++i]);

            ru_info info;

            string shm_name = argv[++i];
            info.shm_name = shm_name;
            if (!pixels.count(shm_name)) {
                int fd = shm_open(shm_name.c_str(), O_CREAT | O_RDWR, 0600);
                if (fd < 0) {
                    perror("shm_open()");
                    return 1;
                }
                ftruncate(fd, SIZE);

                pixels[shm_name] = (uint32_t *)mmap(0, SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
            }

            string flipped = argv[++i];
            info.flipped = (flipped == "1" || flipped == "true");

            ru_infos[ru] = info;
        }
        else if (current_arg == "-f") {
            if (argc - i < 2) {
                printf("Too few arguments for input file (-f)");
                return 1;
            }
            filename = argv[++i];
        }
        else if (current_arg == "-o") {
            if (argc - i < 2) {
                printf("Too few arguments for eof offset (-o)");
                return 1;
            }
            eof_offset = atol(argv[++i]);
        }
        else if (current_arg == "-v") {
            verbose = true;
        }
        else if (current_arg == "-h" || current_arg == "--help") {
            printf("Usage: %s [-v] [-o eof_offset] [--ru ru_id shared_memory_name flip_y ...] -f input_file", argv[0]);
            return 0;
        }
    }


    infile.open(filename.c_str());
    if(!infile.good()){
        printf("Raw file %s not found. Exiting.\n", filename.c_str());
        return 1;
    }

    infile.seekg(-eof_offset, ios::end);
    long current_position = (long)infile.tellg();
    current_position = current_position - current_position%16;
    infile.seekg(current_position, ios::beg);


    while (true) {
        nextLine();

        if(!trigger && !its && isRDHHeader(rawdata[0], rawdata[1])){
            feeIDLayer = ( rawdata[2] & 0xf0 ) ;
            feeIDGBTLink = (rawdata[2] & 0x0f) ;
            feeIDStaveNumber = rawdata[3] ;

            if (verbose)
                printf("layer = %d, gbtlink = %d, stave = %d\n", feeIDLayer, feeIDGBTLink, feeIDStaveNumber);
        }

        if(!its && isITSHeader(rawdata[LENGTH_DATA-1])){
            its = true;
            for(unsigned int i=0;i<sizeof(allIDs);i++){
                allIDs[i]=0;
            }
        }

        if(!trigger && isTriggerHeader(rawdata[LENGTH_DATA-1])){
            trigger = true;
            its = false;
            alpidecounter = 0;
            nActiveLanes = 0;
            nTriggers++;
            triggerBX_delta = triggerBX;
            triggerBX =  rawdata[2]   ; //+  ( (data[3] & 0xf0) >> 4 );
            triggerBX_delta = (triggerBX - triggerBX_delta) % 0xff  ;
            triggerORBIT = (unsigned int) ((rawdata[4] << 8) + rawdata[5]) ;
            for(unsigned int i=0;i<sizeof(allIDs);i++){
                allIDs[i]=0;
            }
        }
        if(!its && trigger && isTriggerTrailer(rawdata[LENGTH_DATA-1])){
            trigger = false;
            for(unsigned int j = 0;j<sizeof(allIDs);j++){
                if(allIDs[j] == 0) continue;
                unsigned int i=0;
                unsigned int nBytes = 0;
                for(i=0;i<alpidecounter/LENGTH_DATA;i++){
                    if(allAlpides[(i+1)*LENGTH_DATA-1]==allIDs[j]){
                        memcpy(&alpide[nBytes], &allAlpides[i*LENGTH_DATA], LENGTH_DATA-1);
                        nBytes+= LENGTH_DATA-1;
                    }
                }
                readALPIDEData(nBytes);
            }
        }
        
        if(!its && trigger){
            if( isALPIDEEmptyFrame(rawdata[0]) ){
            }
            else if( isALPIDEHeader(rawdata[0]) ){
                memcpy(&allAlpides[alpidecounter], &rawdata[0], LENGTH_DATA);       
                allIDs[alpidecounter/LENGTH_DATA] = rawdata[LENGTH_DATA-1]; 
                alpidecounter+=LENGTH_DATA;
            }
            else if( !isALPIDEHeader(rawdata[0]) && !isTriggerHeader(rawdata[LENGTH_DATA-1]) ){
                memcpy(&allAlpides[alpidecounter], &rawdata[0], LENGTH_DATA);       
                alpidecounter+=LENGTH_DATA;
            }
        }
    }
    infile.close();
    return 0;
}

void nextLine() {
    long current_position = (long)infile.tellg();

    infile.read( (char*)( &rawdata[0] ), LENGTH_DATA );
    infile.read( (char*)( &zero[0] ), LENGTH_ZERO );

    while (infile.eof() && infile.fail()) {
        usleep(1000);
        infile.clear();
        infile.seekg(current_position);
        infile.read( (char*)( &rawdata[0] ), LENGTH_DATA );
        infile.read( (char*)( &zero[0] ), LENGTH_ZERO );
    }
}


void readALPIDEData(unsigned int length){

    lengthAlpideData = length;
    readALPIDEData();
}

void readALPIDEData(){

    bool header = false;
    int dataword = 0;
    bool regionheader = false;

    uint8_t headermask = 0xf0;
    uint8_t trailermask = 0xf0;
    uint8_t chipidmask = 0x0f;
    uint8_t regionheadermask = 0xe0;
    uint8_t regionmask = 0x1f;
    uint8_t datamask = 0xc0;
    uint8_t encodermask = 0x3c;
    uint8_t address1mask = 0x03;
    uint8_t address2mask = 0xff;

    uint8_t chipid = 0;
    uint8_t bxcounter = 0;
    uint8_t region = 0;

    if(lengthAlpideData == 0){
        lengthAlpideData = sizeof(alpide);
    }

    //printf("%d\n", lengthAlpideData);
    for(unsigned int i=0;i<lengthAlpideData;i++){
        if(dataword>0) {
            dataword--;
        }
        //printf("%02x ", alpide[i]);
        if(!header && !dataword && ( (headermask & alpide[i])==0xa0 ) ){

            header = true;
            chipid = chipidmask & alpide[i];
            //printf(" --> word %d: header found for chip id %d\n", i, chipid);

            i++;
            bxcounter = alpide[i];
            //printf("%02x", alpide[i]);
            //printf(" --> word %d: bxcounter is %d", i, bxcounter);

        }
        if(header && !dataword){
            if( (regionheadermask & alpide[i]) == 0xc0){
                regionheader = true;
                region = (regionmask & alpide[i]);
                //printf(" --> word %d: region header found for region %d", i, region);
            }
            if( (trailermask & alpide[i]) == 0xb0){
                //break;
                //printf(" --> word %02x: trailer found\n", alpide[i]);
                return;
            }
        }
        if(regionheader && header && !dataword){
            if( (datamask & alpide[i]) == 0x40){
                ////printf(" --> word %d: short data found", i);
                //printf("Filling short %02x %02x\n", alpide[i], alpide[i+1]);
                dataword = 2;
                uint8_t encoder  = (encodermask & alpide[i]) >> 2;
                uint16_t address = (address1mask & alpide[i]);
                address = address << 8;
                uint16_t a = (address2mask & alpide[++i]);
                //printf("%02x ", alpide[i]);
                ////printf("address: %d %02x %d", address, a, a+address);
                address+=a;
                //++i;
                ////printf("%02x ", alpide[++i]);
                int y = getAlpideY(address);
                int x = getAlpideX(region, encoder, address);
                //printf(" --> short data found with encoder %d, address %d, and hitmap 0x%02x", encoder, address, alpide[i]);
                //printf("\n--> position: (x = %d, y = %d)", x, y);
                dataword = 0;
                alpideX = x;
                alpideY = y;
                if (verbose)
                    printf("%d,%d\n", chip_to_world_x(feeIDGBTLink, chipid, x), chip_to_world_y(feeIDGBTLink, chipid, y));
                //pixels[chip_to_world_y(feeIDGBTLink, chipid, y) * resx + chip_to_world_x(feeIDGBTLink, chipid, x) ] = 0xffffffff;
                drawHit(feeIDGBTLink,chip_to_world_x(feeIDGBTLink, chipid, x),chip_to_world_y(feeIDGBTLink, chipid, y),0xffffffff);

                alpideID = chipid;
                alpideBX = bxcounter;
            }
            else if( (datamask & alpide[i]) == 0x00){
                //printf("Filling long %02x\n", alpide[i]);
                uint8_t encoder  = (encodermask & alpide[i]) >> 2;
                //uint8_t encoder  = (encodermask & alpide[i]) ;
                uint16_t address = (address1mask & alpide[i]);
                address = address << 8;
                uint16_t a = (address2mask & alpide[++i]);
                //printf("%02x ", alpide[i]);
                ////printf("address: %d %02x %d", address, a, a+address);
                address+=a;
                i++;
                //printf("%02x ", alpide[i]);
                int y = getAlpideY(address);
                int x = getAlpideX(region, encoder, address);
                //printf(" --> %d long data found with encoder %d, address %d, and hitmap 0x%02x", lengthAlpideData, encoder, address, alpide[i]);
                //printf("\n--> position: (x = %d, y = %d)", x, y);
                alpideX = x;
                alpideY = y;
                if (verbose)
                    printf("%d,%d\n", chip_to_world_x(feeIDGBTLink, chipid, x), chip_to_world_y(feeIDGBTLink, chipid, y));
                drawHit(feeIDGBTLink,chip_to_world_x(feeIDGBTLink, chipid, x),chip_to_world_y(feeIDGBTLink, chipid, y),0xffffffff);

                alpideID = chipid;
                alpideBX = bxcounter;

                for(int h=0;h<8;h++){
                    address++;
                    if( alpide[i] & (1 << h) ){
                        int y = getAlpideY(address);
                        int x = getAlpideX(region, encoder, address);
                        //printf("\n--> position: (x = %d, y = %d)", x, y);
                        //printf("Filling tree...\n");
                        alpideX = x;
                        //if(alpideX == 1024) {
                        //    for(unsigned int i=0;i<lengthAlpideData;i++){
                        //        printf("%02x ", alpide[i]);
                        //    }
                        //    printf(" ##\n");
                        //}
                        alpideY = y;
                        if (verbose)
                            printf("%d,%d\n", chip_to_world_x(feeIDGBTLink, chipid, x), chip_to_world_y(feeIDGBTLink, chipid, y));
                        drawHit(feeIDGBTLink,chip_to_world_x(feeIDGBTLink, chipid, x),chip_to_world_y(feeIDGBTLink, chipid, y),0xffffffff);
                        alpideID = chipid;
                        alpideBX = bxcounter;
                    }
                }

                dataword = 0;
            }
        }
        else {
            //regionheader = false;
            //header = false;
            //dataword = false;
        }
    }
}

int getAlpideY(uint16_t address){
    return address/2;
}

int getAlpideX(uint8_t region, uint8_t encoder, uint16_t address){
    int x = region*32 + encoder*2;
    if(address%4==1) x++;
    if(address%4==2) x++;
    return x;
}



int chip_to_world_x(int ru, int chipid, int x) {
    int world_x = x + (2 - (chipid % 3)) * 1024;
    return world_x;
}

int chip_to_world_y(int ru, int chipid, int y) {
    if (!ru_infos.count(ru)) {
        if (verbose) printf("Found unconfigured readout unit.");
        return 0;
    }
    ru_info info = ru_infos[ru];
    if (info.flipped) {
        y = 511 - y;
    }
    else {
        y += 512;
    }
    int world_y = y + chipid/3 * 1024;
    return world_y;
}
