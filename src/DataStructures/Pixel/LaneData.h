#ifndef LANEDATA
#define LANEDATA

#define MAX_LANEDATA_SIZE 0x10000
#define N_LANES 28

#define MAX_TRIGGERS_PER_PACKET 256

#define LENGTH_DATA 9
#define LENGTH_IDENTIFIER 1
#define LENGTH_PADDING 6

#include <cstdint>
//#include "RawDataParser.h"

struct LaneData {
    unsigned int size;
    unsigned int lane;
    unsigned char laneData[MAX_LANEDATA_SIZE];
};

struct TriggerData {
    uint64_t trigger[MAX_TRIGGERS_PER_PACKET];
    int position[MAX_TRIGGERS_PER_PACKET];
    int size;
};

uint64_t TriggerForByte(TriggerData td, int pos);


#endif
