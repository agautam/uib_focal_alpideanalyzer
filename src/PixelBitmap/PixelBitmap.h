#ifndef PIXELMASKBITMAP
#define PIXELMASKBITMAP

#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <malloc.h>
#include <vector>
#include <cstring>
#include <string>

#include "../Geometry/Pixel.h"

#define _NMaskTreshold 100
#define _height 512
#define _width 1024
#define _bitsperpixel 1 //
#define _planes 1
#define _compression 0
#define _pixelbytesize _height*_width*_bitsperpixel/8
#define _filesize _pixelbytesize+sizeof(bitmap)
#define _xpixelpermeter 0x130B //2835 , 72 DPI
#define _ypixelpermeter 0x130B //2835 , 72 DPI
#define _numcolorspallette 2
#define pixel 0xff //0xFF
#pragma pack(push,1)
typedef struct{
    uint8_t signature[2];
    //char signature[2];
    uint32_t filesize;
    uint32_t reserved;
    uint32_t fileoffset_to_pixelarray;
} bitmapfileheader;
typedef struct{
    uint32_t dibheadersize;
    uint32_t width;
    uint32_t height;
    uint16_t planes;
    uint16_t bitsperpixel;
    uint32_t compression;
    uint32_t imagesize;
    uint32_t ypixelpermeter;
    uint32_t xpixelpermeter;
    uint32_t numcolorspallette;
    uint32_t mostimpcolor;
} bitmapinfoheader;
typedef struct {
    uint8_t red;
    uint8_t green;
    uint8_t blue;
    uint8_t other;
} bitmapcolortable;
typedef struct {
    bitmapfileheader fileheader;
    bitmapinfoheader infoheader;
    bitmapcolortable colortable[2];
} bitmap;
uint8_t maskedpixels[_pixelbytesize];
#pragma pack(pop)

class PixelBitmap {

    public:
        PixelBitmap(int ru, int id);
        int write(bool dummy = false);
        int setPixels(std::vector<Pixel> _px);
	void setChipId(int id);
	int getChipId();

        
    private:
        int generateFileHeader();
        int generateInfoHeader();
        int setColorTable();
        int writePixelMap();
        int writeDummyPixelMap();

        std::vector<Pixel> px;

        FILE *fp;
        bitmap *pbitmap;
        uint8_t *pixelbuffer;
        std::string bitmapname;

	int chipid;

};

//int writeNoisyPixelBitmap(int id, std::vector<noisyPixel> noisyPixels);
//int createPixelMaskMap(std::vector<noisyPixel> noisyPixels);
#endif
