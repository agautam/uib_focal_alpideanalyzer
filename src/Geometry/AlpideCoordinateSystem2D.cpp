#include "AlpideCoordinateSystem2D.h"

AlpideCoordinateSystem2D::AlpideCoordinateSystem2D(){

}

AlpideCoordinateSystem2D::AlpideCoordinateSystem2D(int _offx, int _offy, bool _invx, bool _invy){
    px_x_offset = _offx;
    px_y_offset = _offy;
    x_axis_inverted = _invx;
    y_axis_inverted = _invy;
}

void AlpideCoordinateSystem2D::HitLocal2Global(std::pair<unsigned short, unsigned short> &localpx, std::pair<unsigned short, unsigned short> &globalpx){
    unsigned short x;
    unsigned short y;

    if(x_axis_inverted){
        x = ALPIDE_COL_MAX - localpx.first;
    } else{
        x = localpx.first;
    }

    if(y_axis_inverted){
        y = ALPIDE_ROW_MAX - localpx.second;
    } else{
        y = localpx.second;
    }

    x += px_x_offset;
    y += px_y_offset;

    globalpx.first = x;
    globalpx.second = y;

}

void AlpideCoordinateSystem2D::HitGlobal2Local(std::pair<unsigned short, unsigned short> &globalpx, std::pair<unsigned short, unsigned short> &localpx){
    unsigned short x;
    unsigned short y;

    x = globalpx.first - px_x_offset;
    y = globalpx.second - px_y_offset;

    if(x_axis_inverted){
        x = ALPIDE_COL_MAX - globalpx.first;
    } else{
        x = localpx.first;
    }

    if(y_axis_inverted){
        y = ALPIDE_ROW_MAX - globalpx.second;
    } else{
        y = globalpx.second;
    }

    localpx.first = x;
    localpx.second = y;

}

int AlpideCoordinateSystem2D::GetX0(){return px_x_offset;}
int AlpideCoordinateSystem2D::GetY0(){return px_y_offset;}