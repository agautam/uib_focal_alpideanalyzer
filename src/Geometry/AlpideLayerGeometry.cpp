#include <cstdio>

#include "AlpideLayerGeometry.h"
#include "AlpideCoordinateSystem2D.h"

AlpideCoordinateSystem3D *AlpideLayerGeometry::GetAlpideCoordinates(unsigned short _fee, unsigned short _id){
    std::pair<unsigned short, unsigned short> address = {_fee, _id};
    auto it = alpides.find(address);
    if(it == alpides.end()){
    	printf("WARNING: Accessing alpide fee %d id %d which does not exist in the current geometry\n", _fee, _id);
	return nullptr;
    }
    return it->second;
}

unsigned int AlpideLayerGeometry::GetNAlpides(){
    return nAlpides;
}

unsigned int  AlpideLayerGeometry::GetNAlpidesActive(){
    return nAlpidesActive;
}

double AlpideLayerGeometry::GetXmax(){return xmax;}
double AlpideLayerGeometry::GetXmin(){return xmin;}
double AlpideLayerGeometry::GetYmax(){return ymax;}
double AlpideLayerGeometry::GetYmin(){return ymin;}


void AlpideLayerGeometry::SetSPSBeamTest2021Geometry(){

    name = "SPSBeamTest2021Geometry";

    nAlpides = 36;
    nAlpidesActive = 21;
    
    xmin = 0;
    ymin = 0;
    xmax = 3*AlpideMatrix::N_ALPIDE_COLS;
    ymax = 6*AlpideMatrix::N_ALPIDE_ROWS;

// Fee 0
    alpides[{0,0}] = new AlpideCoordinateSystem3D(5,2*AlpideMatrix::N_ALPIDE_COLS,4*AlpideMatrix::N_ALPIDE_ROWS,true,false);
    alpides[{0,1}] = new AlpideCoordinateSystem3D(5,1*AlpideMatrix::N_ALPIDE_COLS,4*AlpideMatrix::N_ALPIDE_ROWS,true,false);
    alpides[{0,2}] = new AlpideCoordinateSystem3D(5,0*AlpideMatrix::N_ALPIDE_COLS,4*AlpideMatrix::N_ALPIDE_ROWS,true,false);

    alpides[{0,3}] = new AlpideCoordinateSystem3D(5,2*AlpideMatrix::N_ALPIDE_COLS,2*AlpideMatrix::N_ALPIDE_ROWS,true,false);
    alpides[{0,4}] = new AlpideCoordinateSystem3D(5,1*AlpideMatrix::N_ALPIDE_COLS,2*AlpideMatrix::N_ALPIDE_ROWS,true,false);
    alpides[{0,5}] = new AlpideCoordinateSystem3D(5,0*AlpideMatrix::N_ALPIDE_COLS,2*AlpideMatrix::N_ALPIDE_ROWS,true,false);

    alpides[{0,6}] = new AlpideCoordinateSystem3D(5,2*AlpideMatrix::N_ALPIDE_COLS,0*AlpideMatrix::N_ALPIDE_ROWS,true,false);
    alpides[{0,7}] = new AlpideCoordinateSystem3D(5,1*AlpideMatrix::N_ALPIDE_COLS,0*AlpideMatrix::N_ALPIDE_ROWS,true,false);
    alpides[{0,8}] = new AlpideCoordinateSystem3D(5,0*AlpideMatrix::N_ALPIDE_COLS,0*AlpideMatrix::N_ALPIDE_ROWS,true,false);

// Fee 2
    alpides[{2,0}] = new AlpideCoordinateSystem3D(10,2*AlpideMatrix::N_ALPIDE_COLS,4*AlpideMatrix::N_ALPIDE_ROWS,true,false);
    alpides[{2,1}] = new AlpideCoordinateSystem3D(10,1*AlpideMatrix::N_ALPIDE_COLS,4*AlpideMatrix::N_ALPIDE_ROWS,true,false);
    alpides[{2,2}] = new AlpideCoordinateSystem3D(10,0*AlpideMatrix::N_ALPIDE_COLS,4*AlpideMatrix::N_ALPIDE_ROWS,true,false);

    alpides[{2,3}] = new AlpideCoordinateSystem3D(10,2*AlpideMatrix::N_ALPIDE_COLS,2*AlpideMatrix::N_ALPIDE_ROWS,true,false);
    alpides[{2,4}] = new AlpideCoordinateSystem3D(10,1*AlpideMatrix::N_ALPIDE_COLS,2*AlpideMatrix::N_ALPIDE_ROWS,true,false);
    alpides[{2,5}] = new AlpideCoordinateSystem3D(10,0*AlpideMatrix::N_ALPIDE_COLS,2*AlpideMatrix::N_ALPIDE_ROWS,true,false);

    alpides[{2,6}] = new AlpideCoordinateSystem3D(10,2*AlpideMatrix::N_ALPIDE_COLS,0*AlpideMatrix::N_ALPIDE_ROWS,true,false);
    alpides[{2,7}] = new AlpideCoordinateSystem3D(10,1*AlpideMatrix::N_ALPIDE_COLS,0*AlpideMatrix::N_ALPIDE_ROWS,true,false);
    alpides[{2,8}] = new AlpideCoordinateSystem3D(10,0*AlpideMatrix::N_ALPIDE_COLS,0*AlpideMatrix::N_ALPIDE_ROWS,true,false);

// Fee 1
    alpides[{1,0}] = new AlpideCoordinateSystem3D(5,2*AlpideMatrix::N_ALPIDE_COLS,5*AlpideMatrix::N_ALPIDE_ROWS,true,true);
    alpides[{1,1}] = new AlpideCoordinateSystem3D(5,1*AlpideMatrix::N_ALPIDE_COLS,5*AlpideMatrix::N_ALPIDE_ROWS,true,true);
    alpides[{1,2}] = new AlpideCoordinateSystem3D(5,0*AlpideMatrix::N_ALPIDE_COLS,5*AlpideMatrix::N_ALPIDE_ROWS,true,true);

    alpides[{1,3}] = new AlpideCoordinateSystem3D(5,2*AlpideMatrix::N_ALPIDE_COLS,3*AlpideMatrix::N_ALPIDE_ROWS,true,true);
    alpides[{1,4}] = new AlpideCoordinateSystem3D(5,1*AlpideMatrix::N_ALPIDE_COLS,3*AlpideMatrix::N_ALPIDE_ROWS,true,true);
    alpides[{1,5}] = new AlpideCoordinateSystem3D(5,0*AlpideMatrix::N_ALPIDE_COLS,3*AlpideMatrix::N_ALPIDE_ROWS,true,true);

    alpides[{1,6}] = new AlpideCoordinateSystem3D(5,2*AlpideMatrix::N_ALPIDE_COLS,1*AlpideMatrix::N_ALPIDE_ROWS,true,true);
    alpides[{1,7}] = new AlpideCoordinateSystem3D(5,1*AlpideMatrix::N_ALPIDE_COLS,1*AlpideMatrix::N_ALPIDE_ROWS,true,true);
    alpides[{1,8}] = new AlpideCoordinateSystem3D(5,0*AlpideMatrix::N_ALPIDE_COLS,1*AlpideMatrix::N_ALPIDE_ROWS,true,true);

// Fee 3
    alpides[{3,0}] = new AlpideCoordinateSystem3D(10,2*AlpideMatrix::N_ALPIDE_COLS,5*AlpideMatrix::N_ALPIDE_ROWS,true,true);
    alpides[{3,1}] = new AlpideCoordinateSystem3D(10,1*AlpideMatrix::N_ALPIDE_COLS,5*AlpideMatrix::N_ALPIDE_ROWS,true,true);
    alpides[{3,2}] = new AlpideCoordinateSystem3D(10,0*AlpideMatrix::N_ALPIDE_COLS,5*AlpideMatrix::N_ALPIDE_ROWS,true,true);

    alpides[{3,3}] = new AlpideCoordinateSystem3D(10,2*AlpideMatrix::N_ALPIDE_COLS,3*AlpideMatrix::N_ALPIDE_ROWS,true,true);
    alpides[{3,4}] = new AlpideCoordinateSystem3D(10,1*AlpideMatrix::N_ALPIDE_COLS,3*AlpideMatrix::N_ALPIDE_ROWS,true,true);
    alpides[{3,5}] = new AlpideCoordinateSystem3D(10,0*AlpideMatrix::N_ALPIDE_COLS,3*AlpideMatrix::N_ALPIDE_ROWS,true,true);

    alpides[{3,6}] = new AlpideCoordinateSystem3D(10,2*AlpideMatrix::N_ALPIDE_COLS,1*AlpideMatrix::N_ALPIDE_ROWS,true,true);
    alpides[{3,7}] = new AlpideCoordinateSystem3D(10,1*AlpideMatrix::N_ALPIDE_COLS,1*AlpideMatrix::N_ALPIDE_ROWS,true,true);
    alpides[{3,8}] = new AlpideCoordinateSystem3D(10,0*AlpideMatrix::N_ALPIDE_COLS,1*AlpideMatrix::N_ALPIDE_ROWS,true,true);


    for(auto const a : alpides){

        printf("fee %d, id %d -> X0 = %d, Y0 = %d, Layer %d\n", a.first.first, a.first.second, a.second->GetX0(), a.second->GetY0(), a.second->GetLayer());
    }
}
std::string AlpideLayerGeometry::GetName(){
	return name;
}	
