#include "AlpideDecoder.h"

#include <stdint.h>
#include <stdio.h>
#include <iostream>
#include <cstdint>

#include "TTree.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TKey.h"
#include "RawDataParser.h"
#include "TFile.h"
#include "AlpideCoordinateSystem3D.h"

#include <vector>
//using namespace alpide_decoder;

int AlpideDecoder::NUMBER_OF_BUSIES = 0;
int AlpideDecoder::NUMBER_OF_BUSY_VIOLATIONS = 0;

AlpideDataType AlpideDecoder::GetDataType(unsigned char dataWord) {

  //native alpide words
  if (dataWord == 0xff)               return ALPIDE_IDLE;
  else if (dataWord == 0xf1)          return ALPIDE_BUSYON;
  else if (dataWord == 0xf0)          return ALPIDE_BUSYOFF;
  else if ((dataWord & 0xf0) == 0xa0) return ALPIDE_CHIPHEADER;
  else if ((dataWord & 0xf0) == 0xb0) return ALPIDE_CHIPTRAILER;
  else if ((dataWord & 0xf0) == 0xe0) return ALPIDE_EMPTYFRAME;
  else if ((dataWord & 0xe0) == 0xc0) return ALPIDE_REGIONHEADER;
  else if ((dataWord & 0xc0) == 0x40) return ALPIDE_DATASHORT;
  else if ((dataWord & 0xc0) == 0x00) return ALPIDE_DATALONG;
  //alpide protocol extension words
  //else if(dataWord == 0x00)           return ALPIDE_EXTENSION_LANE_PADDING;
  else if(dataWord == 0xf2)           return ALPIDE_EXTENSION_LANE_ENTERING_STRIP_DATA_MODE;    
  else if(dataWord == 0xf3)           return ALPIDE_EXTENSION_LANE_EXITING_STRIP_DATA_MODE;    
  else if(dataWord == 0xf4)           return ALPIDE_EXTENSION_DETECTOR_TIMEOUT;
  else if(dataWord == 0xf5)           return ALPIDE_EXTENSION_OOT_ERROR_CRITICAL;
  else if(dataWord == 0xf6)           return ALPIDE_EXTENSION_PROTOCOL_ERROR;
  else if(dataWord == 0xf7)           return ALPIDE_EXTENSION_LANE_FIFO_OVERFLOW_ERROR;
  else if(dataWord == 0xf8)           return ALPIDE_EXTENSION_FSM_ERROR;
  else if(dataWord == 0xf9)           return ALPIDE_EXTENSION_PENDING_DETECTOREVENTS_LIMITS;
  else if(dataWord == 0xfa)           return ALPIDE_EXTENSION_PENDING_LANEEVENT_LIMITS;
  else if(dataWord == 0xfb)           return ALPIDE_EXTENSION_LANE_PROTOCOL_ERROR;
  else if(dataWord == 0xfe)           return ALPIDE_EXTENSION_OOT_ERROR_NON_CRITICAL;

  // something crazy we don't know
  else return ALPIDE_UNKNOWN;
}

bool AlpideDecoder::IsFatal(unsigned char word){
  AlpideDataType fataltype = GetDataType(word);
  if(fataltype == ALPIDE_EXTENSION_DETECTOR_TIMEOUT)              return true;
  else if(fataltype == ALPIDE_EXTENSION_OOT_ERROR_CRITICAL)       return true;
  else if(fataltype == ALPIDE_EXTENSION_PROTOCOL_ERROR)           return true;
  else if(fataltype == ALPIDE_EXTENSION_LANE_FIFO_OVERFLOW_ERROR) return true;
  else if(fataltype == ALPIDE_EXTENSION_FSM_ERROR)                return true;
  else if(fataltype == ALPIDE_EXTENSION_PENDING_DETECTOREVENTS_LIMITS) return true;
  else if(fataltype == ALPIDE_EXTENSION_PENDING_LANEEVENT_LIMITS) return true;
  else if(fataltype == ALPIDE_EXTENSION_LANE_PROTOCOL_ERROR)      return true;
  else return false;
}

bool AlpideDecoder::RestOfLaneIsPadding(){
  unsigned int byteindex;
  for(unsigned int i=1;i<LENGTH_DATA;i++){
    
    byteindex = byte+i;

    if(byteindex>= byteMax) break;  // not possible, end of data reached
    //if(!(byteindex%LENGTH_DATA)) break; // start of lane data, break;

    if(data.laneData[byteindex]!=0) return false;

  }
  return true;
}

void AlpideDecoder::initFiles(TFile *file, TFile *_histfile){

  gInterpreter->GenerateDictionary("vector<pair<uint16_t,uint16_t>>", "vector", "");
  gInterpreter->GenerateDictionary("vector<pair<unsigned short,unsigned short>>", "vector", "");
  histfile = _histfile;
  treefile = file;
  treefile->cd();
  
  pixelhittree = new TTree("HITS", Form("x-y pixel hits in alpide"));
  pixelhittree->Branch("trigger", &trigger);
  pixelhittree->Branch("feeid", &feeid);
  pixelhittree->Branch("alpideid", &chipid);
  pixelhittree->Branch("pixelhits", &pixelhits);
  pixelhittree->Branch("globalpixelhits", &globalpixelhits);
  pixelhittree->Branch("headerflag", &headerflag);
  pixelhittree->Branch("trailerflag", &trailerflag);
  pixelhittree->SetMaxTreeSize(maxtreesize);

  histfile->cd();
  fataltree = new TTree("FATAL", Form("alpide protocol extension FATAL words"));
  fataltree->Branch("trigger", &trigger);
  fataltree->Branch("feeid", &feeid);
  fataltree->Branch("alpideid", &chipid);
  fataltree->Branch("fatal", &fatal);

  printf("AlpideDecoder ROOT file and trees initialized\n");

}

AlpideDecoder::AlpideDecoder(){


  //TInterpreter gInterpreter = new TInterpreter();
  //gInterpreter->GenerateDictionary("vector<pair<uint16_t,uint16_t>>", "vector");
  gInterpreter->GenerateDictionary("vector<pair<uint16_t,uint16_t>>", "vector", "");
  gInterpreter->GenerateDictionary("vector<pair<unsigned short,unsigned short>>", "vector", "");

  trigger = 0;
  feeid = 0;
  chipid = 0;
  headerflag = 0;
  trailerflag = 0;
  pixelhits = std::vector<std::pair<uint16_t, uint16_t> >();
  globalpixelhits = std::vector<std::pair<uint16_t, uint16_t> >();
  lpx = std::pair<uint16_t, uint16_t>();
  gpx = std::pair<uint16_t, uint16_t>();


  printf("AlpideDecoder created.\n");
}

void AlpideDecoder::SetFeeId(unsigned int _f){
  feeid = _f;
}

void AlpideDecoder::SetOrbit(uint64_t _orbit){
  orbit = _orbit;
}

void AlpideDecoder::SetTrigger(uint64_t _t){
  trigger = _t;
}

bool AlpideDecoder::NextByte(){
  byte++;
  if( byte<data.size &&
      //!(byte%LENGTH_DATA) && // check if byte is first char in lane
      IsFatal(data.laneData[byte]) && // check if there is a fatal word
      RestOfLaneIsPadding()
  ){
    //fatal = (data.laneData[byte] & 0x0f);
    fatal = data.laneData[byte] ;
    //printf("%d", byte);
    mFatal = true;
    PrintFatalMessage(GetDataType(data.laneData[byte]));
    return true;
  }
  return (byte<byteMax);
}

void AlpideDecoder::PrintFatalMessage(AlpideDataType t){
  switch(t){
    case(ALPIDE_EXTENSION_DETECTOR_TIMEOUT):
      printf("FATAL: Detector Timeout 0x%02x, feeid %d, chipid %d, lane %d, trigger 0x%08llu!\n", data.laneData[byte], feeid, chipid, data.lane, trigger);
      break;
    case(ALPIDE_EXTENSION_OOT_ERROR_CRITICAL):
      printf("FATAL: 8b10b Out-of-table (critical) 0x%02x, feeid %d, chipid %d, lane %d, trigger 0x%08llu!\n", data.laneData[byte], feeid, chipid, data.lane, trigger);
      break;
    case(ALPIDE_EXTENSION_LANE_FIFO_OVERFLOW_ERROR):
      printf("FATAL: Lane FIFO Overflow 0x%02x, feeid %d, chipid %d, lane %d, trigger 0x%08llu!\n", data.laneData[byte], feeid, chipid, data.lane, trigger);
      break;
    case(ALPIDE_EXTENSION_FSM_ERROR):
      printf("FATAL: FSM Error 0x%02x, feeid %d, chipid %d, lane %d, trigger 0x%08llu!\n", data.laneData[byte], feeid, chipid, data.lane, trigger);
      break;
    case(ALPIDE_EXTENSION_PENDING_DETECTOREVENTS_LIMITS):
      printf("FATAL: Pending Detector Events Limit 0x%02x, feeid %d, chipid %d, lane %d, trigger 0x%08llu!\n", data.laneData[byte], feeid, chipid, data.lane, trigger);        
      break;
    case(ALPIDE_EXTENSION_PENDING_LANEEVENT_LIMITS):
      printf("FATAL: Pending Lane Events Limit 0x%02x, feeid %d, chipid %d, lane %d, trigger 0x%08llu!\n", data.laneData[byte], feeid, chipid, data.lane, trigger);        
      break;
    case(ALPIDE_EXTENSION_LANE_PROTOCOL_ERROR):
      printf("FATAL: Lane Protocol Error 0x%02x, feeid %d, chipid %d, lane %d, trigger 0x%08llu!\n", data.laneData[byte], feeid, chipid, data.lane, trigger);        
      break;
    case(ALPIDE_EXTENSION_PROTOCOL_ERROR):
      printf("FATAL: Alpide Protocol Error 0x%02x, feeid %d, chipid %d, lane %d, trigger 0x%08llu!\n", data.laneData[byte], feeid, chipid, data.lane, trigger);        
      break;
    default:
      printf("FATAL: Unknown Message 0x%02x, feeid %d, chipid %d, lane %d, trigger 0x%08llu!\n", data.laneData[byte], feeid, chipid, data.lane, trigger);        

      break;
  }
}

bool AlpideDecoder::DecodeEvent(LaneData &_data, TriggerData triggerdata)
    //unsigned char *data, 
    //int nBytes,
    /*std::vector<std::pair<uint16_t,uint16_t>> &hits,*/
    //unsigned int &bunch_cnt, int &prioErrors)
{

  treefile->cd();
  data = _data;

  pixelhits.clear();

  byte=0;
  byteMax = data.size;
  int region;

  bool done = false;
  bool started = false;
  mFatal = false;

  while(byte < byteMax){

    type = GetDataType(data.laneData[byte]);
    //if(data.lane==23) {
    //      printf("0x%02x ", data.laneData[byte]);
    //      if(!(byte %20)) printf("\n");
    //}
    switch(type){
      // header
      case(ALPIDE_CHIPHEADER):
        trigger = TriggerForByte(triggerdata, byte);
        //printf("alpide starting with alpide header, chip id %d, trigger 0x%08llux (%llu) \n", chipid, trigger, trigger);
        started = true;
        done = false;
        mFatal = false;
        chipid = (uint8_t) (data.laneData[byte] & 0x0f);
        headerflag = (uint8_t) ((data.laneData[byte] & 0xf0) >> 4);
        byte++;
        byte++;
	
	if(mUseGlobalGeometry){
		coordinates = geometry->GetAlpideCoordinates(feeid, chipid);
	}
	CreateROOTObjects();            

        break;
      // empty frame
      case(ALPIDE_EMPTYFRAME):
        trigger = TriggerForByte(triggerdata, byte);
        //printf("alpide empty frame, chip id %d, trigger 0x%08llux (%llu)\n", chipid, trigger, trigger);
        started = true;
        done = true;
        mFatal = false;
        chipid = (uint8_t) (data.laneData[byte] & 0x0f);
        headerflag = (uint8_t) ((data.laneData[byte] & 0xf0) >> 4);
	
	if(mUseGlobalGeometry){
		coordinates = geometry->GetAlpideCoordinates(feeid, chipid);
	}
	CreateROOTObjects();
        
	Fill();
        
	byte+=9;
        break;
      // trailer found
      case(ALPIDE_CHIPTRAILER):
        started = false;
        trailerflag =  (uint8_t) (data.laneData[byte] & 0x0f);
        done=true;
        byte++;
        //printf("TRAILER found: feeid %d, chipdid %d, lane %d, %lu hits\n", feeid, chipid, data.lane, pixelhits.size());
        //if(trailerflag) {
        //  printf("TRAILER flag 0x%02x is not zero: feeid %d, chipdid %d, lane %d, %lu hits, trigger 0x%08llux\n", data.laneData[byte], feeid, chipid, data.lane, pixelhits.size(), trigger);
        //} 
	      Fill();
        break;
      // region header
      case(ALPIDE_REGIONHEADER):
        region = data.laneData[byte] & 0x1f;
        byte++;
        //printf("alpide %d region %d\n", chipid, region);
        break;
      // data long word
      case(ALPIDE_DATALONG):
        {
          //if(done || fatal) {
          //  byte++;
          //  break;
          //}

          //printf("long %d\n", byte);
          if(!started) {
            NextByte();
            break;
          }

          uint16_t data_field = (((int16_t) data.laneData[byte]) << 8) + data.laneData[byte+1];
          //byte++;
          if(!NextByte()) break;
          uint16_t encoder = (data_field & 0x3c00) >> 10;
          uint16_t address = (data_field & 0x03ff);
          //byte++;
          if(!NextByte()) break;
          uint8_t hitmap = (data.laneData[byte] & 0x7f);
          
          uint16_t y = AlpideY(address);
          uint16_t x = AlpideX(region, encoder, address); 
          lpx = {x,y};
          pixelhits.push_back(lpx);
          
	  if(mUseGlobalGeometry && (coordinates!=nullptr) ){
	  	coordinates->HitLocal2Global(lpx, gpx);
          	globalpixelhits.push_back(gpx);
	  }
          //pixelhitmap->Fill(x, y);

  //#ifdef ALPIDE_DEBUG            
          //printf("region %d, encoder %d, address %d, 0x%02x\n", region, encoder, address, hitmap);
  //#endif
          for(unsigned int h=0;h<7;h++){
            //address++;
            if( (hitmap>>h) & 0x1 ){
                          y = AlpideY(address+h+1);
                          x = AlpideX(region, encoder, address+h+1);
                          lpx = {x,y};
                          pixelhits.push_back(lpx);
                          
	  		  if(mUseGlobalGeometry && (coordinates!=nullptr) ){
			  	geometry->GetAlpideCoordinates(feeid,chipid)->HitLocal2Global(lpx, gpx);
                          	globalpixelhits.push_back(gpx);
			  }
                          //pixelhitmap->Fill(x, y);

  #ifdef ALPIDE_DEBUG                        
                          printf("(%d, %d), long\n", x, y);
  #endif
            }
          }
          //printf("(%d, %d), long %d\n", x, y, byte-2);


          byte++;

          break;
        }
        break;
      // data short
      case(ALPIDE_DATASHORT):
        {
          //if(!started) {
          //  NextByte();
          //  break;
          //}
          uint16_t data_field = (((int16_t) data.laneData[byte]) << 8) + data.laneData[byte+1];
          //byte++;
          if(!NextByte()) break;
          uint16_t encoder = (data_field & 0x3c00) >> 10;
          uint16_t address = (data_field & 0x03ff);
          uint16_t y = AlpideY(address);
          uint16_t x = AlpideX(region, encoder, address);
          lpx = {x,y};
          pixelhits.push_back(lpx);
	  if(mUseGlobalGeometry && (coordinates!=nullptr) ){
	  	geometry->GetAlpideCoordinates(feeid,chipid)->HitLocal2Global(lpx, gpx);
          	globalpixelhits.push_back(gpx);          //pixelhitmap->Fill(x, y);
	  }
  //#ifdef ALPIDE_DEBUG
          //printf("(%d, %d, [%02x %02x]), short\n", x, y, data.laneData[byte-1], data.laneData[byte]);
  //#endif        
          byte++;
          break;
        }
        break;
      // idle
      case(ALPIDE_IDLE):
        byte++;
        break;
      // busy on  
      case(ALPIDE_BUSYON):
        byte++;
        break;
      // busy off  
      case(ALPIDE_BUSYOFF):
        byte++;
        break;
      // unknown
      //case(ALPIDE_LANE_ZERO):
      //  byte++;
      //  break;
      case(ALPIDE_UNKNOWN):
        printf("ERROR: unknown alpide data 0x%02x word detected on lane %d!\n", data.laneData[byte], data.lane);
        byte++;  
        break;
      case(ALPIDE_EXTENSION_DETECTOR_TIMEOUT):
        PrintFatalMessage(ALPIDE_EXTENSION_DETECTOR_TIMEOUT);
        fatal = (data.laneData[byte] & 0x0f);
        mFatal = true;
        byte++;
        break;
      case(ALPIDE_EXTENSION_OOT_ERROR_CRITICAL):
        PrintFatalMessage(ALPIDE_EXTENSION_OOT_ERROR_CRITICAL);
        fatal = (data.laneData[byte] & 0x0f);
        mFatal = true;
        byte++;
        break;  
      case(ALPIDE_EXTENSION_LANE_FIFO_OVERFLOW_ERROR):
        PrintFatalMessage(ALPIDE_EXTENSION_LANE_FIFO_OVERFLOW_ERROR);
        fatal = (data.laneData[byte] & 0x0f);
        mFatal = true;
        byte++;
        break; 
      case(ALPIDE_EXTENSION_FSM_ERROR):
        PrintFatalMessage(ALPIDE_EXTENSION_FSM_ERROR);
        fatal = (data.laneData[byte] & 0x0f);
        mFatal = true;
        byte++;
        break;
      case(ALPIDE_EXTENSION_PENDING_DETECTOREVENTS_LIMITS):
        PrintFatalMessage(ALPIDE_EXTENSION_PENDING_DETECTOREVENTS_LIMITS);
        fatal = (data.laneData[byte] & 0x0f);
        mFatal = true;
        byte++;
        break;   
      case(ALPIDE_EXTENSION_PENDING_LANEEVENT_LIMITS):
        PrintFatalMessage(ALPIDE_EXTENSION_PENDING_LANEEVENT_LIMITS);
        fatal = (data.laneData[byte] & 0x0f);
        mFatal = true;
        byte++;
        break;
      case(ALPIDE_EXTENSION_LANE_PROTOCOL_ERROR):
        PrintFatalMessage(ALPIDE_EXTENSION_LANE_PROTOCOL_ERROR);
        fatal = (data.laneData[byte] & 0x0f);
        mFatal = true;
        byte++;
        break;
      case(ALPIDE_EXTENSION_PROTOCOL_ERROR):
        PrintFatalMessage(ALPIDE_EXTENSION_PROTOCOL_ERROR);
        fatal = (data.laneData[byte] & 0x0f);
        mFatal = true;
        byte++;
        break;
      case(ALPIDE_EXTENSION_OOT_ERROR_NON_CRITICAL):
        printf("ERROR: 8b10b Out-of-table (non-critical) 0x%02x, feeid %d, chipid %d, lane %d, trigger 0x%llu!\n", data.laneData[byte], feeid, chipid, data.lane, trigger);        
        byte++;
        break;
      case(ALPIDE_EXTENSION_LANE_PADDING):
        printf("ERROR: alpide lane padding 0x%02x word detected on lane %d!\n", data.laneData[byte], data.lane);
        byte++;
        break;
      // default   
      default:
        printf("ERROR: unknown alpide data 0x%02x word detected on lane %d!\n", data.laneData[byte], data.lane);
        byte++;
        break;

    }
  }
  if(!done && !mFatal) {
    //printf("ERROR: decoding of alpide data was not finished correctly.\n");
  } else if(mFatal){
    //for(unsigned int i=0;i<data.size+9;i++){
	  //  if(!(i%9)) printf("\n%d: ", i);
	  //  printf("%02x ", data.laneData[i]);
    //}
    //  printf("\n");
    printf("FATAL %02x: Finished decoding feeid %d, chipid %d, lane %d, trigger 0x%08llu\n", fatal, feeid, chipid, data.lane, trigger);

    fatal = (fatal & 0x0f);

    fataltree->Fill();
  } 
  //else { 
    //printf("GOOD %d: Finished decoding feeid %d, chipid %d, lane %d, trigger 0x%08llux\n", done, feeid, chipid, data.lane, trigger);
  //}

  return done;
}

uint16_t AlpideDecoder::AlpideY(uint16_t address){
    return address/2;
}

uint16_t AlpideDecoder::AlpideX(uint8_t region, uint8_t encoder, uint16_t address){
    int x = region*32 + encoder*2;
    if(address%4==1) x++;
    if(address%4==2) x++;
    return x;
}

void AlpideDecoder::WriteToFile(TFile *rootfile){
  treefile = rootfile;
  WriteToFile();
}

void AlpideDecoder::WriteToFile(){
  
  TObject *obj;
  TList *list; 

  histfile->cd();
  list = histfile->GetList();
  TIter nexthist(list);
  
  while((obj = (TObject*)nexthist())){
    if(obj->InheritsFrom("TNamed")) {
      printf("Writing %s %s to file %s\n", obj->ClassName(), obj->GetName(),  histfile->GetName());
      obj->Write();
    }
  }

  treefile->cd();
  list = treefile->GetList();
  TIter nexttree(list);
    
  while((obj = (TObject*)nexttree())){
    if(obj->InheritsFrom("TNamed")) {
      treefile->cd();
      printf("Writing %s %s to file %s\n", obj->ClassName(), obj->GetName(),  treefile->GetName());
      obj->Write();
    }
  }
}


void AlpideDecoder::CreateROOTObjects(){
  histfile->cd();
  if(mUseGlobalGeometry && (coordinates!=nullptr) ){
  	unsigned int layer = geometry->GetAlpideCoordinates(feeid, chipid)->GetLayer();
  	std::string globalhistname = Form("globalpixelhitmap_layer%d", layer);
  	double xmin = geometry->GetXmin();
  	double ymin = geometry->GetYmin();
 	double xmax = geometry->GetXmax();
  	double ymax = geometry->GetYmax();
  	if( (globalpixelhitmap = (TH2D*) histfile->Get(globalhistname.c_str())) ){

  	}else{
    		globalpixelhitmap = new TH2D(
        	globalhistname.c_str(),
        	globalhistname.c_str(),
        	(int) (xmax-xmin), xmin, xmax,
        	(int) (ymax-ymin), ymin, ymax
        	);
    	printf("created new histogram %s in %s with currently %d entries\n", globalpixelhitmap->GetName(), globalpixelhitmap->GetDirectory()->GetName(), (int) globalpixelhitmap->GetEntries() );

  	}
  }
 

  if( (pixelhitmap = (TH2D*) histfile->Get(Form("pixelhitmap_fee%d_id%d", feeid, chipid)) ) ){
    //printf("using existing histogram %s with currently %d entries\n", pixelhitmap->GetName(), (int) pixelhitmap->GetEntries() );
  } else {

    pixelhitmap = new TH2D(
        Form("pixelhitmap_fee%d_id%d", feeid, chipid),
        Form("pixelhitmap_fee%d_id%d", feeid, chipid),
        ALPIDE_N_COLS, 0, ALPIDE_N_COLS,
        ALPIDE_N_ROWS, 0, ALPIDE_N_ROWS
        );

    printf("created new histogram %s in %s with currently %d entries\n", pixelhitmap->GetName(), pixelhitmap->GetDirectory()->GetName(), (int) pixelhitmap->GetEntries() );
  }

  
  if( (numberofhits = (TH1D*) histfile->Get(Form("numberofhits_fee%d_id%d", feeid, chipid)) ) ){
    //printf("using existing histogram %s with currently %d entries\n", pixelhitmap->GetName(), (int) pixelhitmap->GetEntries() );
  } else {


    numberofhits = new TH1D(
      Form("numberofhits_fee%d_id%d", feeid, chipid),
      Form("numberofhits_fee%d_id%d", feeid, chipid),
      ALPIDE_MAXIMUM_HITS, 0, ALPIDE_MAXIMUM_HITS
    );  
    printf("created new histogram %s in %s with currently %d entries\n", numberofhits->GetName(), numberofhits->GetDirectory()->GetName(), (int) numberofhits->GetEntries() );

  }
}

void AlpideDecoder::Fill(){
	
  treefile->cd();
  pixelhittree->Fill();
  treefile = pixelhittree->GetDirectory()->GetFile();

  histfile->cd();	
  numberofhits->Fill(pixelhits.size());
  for(std::pair<uint16_t, uint16_t> hit : pixelhits ){
    pixelhitmap->Fill(hit.first, hit.second);
  }
  pixelhits.clear();
  for(std::pair<uint16_t, uint16_t> hit : globalpixelhits ){
    globalpixelhitmap->Fill(hit.first, hit.second);
  }
  globalpixelhits.clear();
}


void AlpideDecoder::SetTreeFile(TFile *rootfile){
	treefile = rootfile;
}


void AlpideDecoder::SetAlpideLayerGeometry(AlpideLayerGeometry *coordsystem){
  mUseGlobalGeometry = true;
  geometry = coordsystem;
  printf("Created geometry %s with %d alpides (%d active)\n", geometry->GetName().c_str(), geometry->GetNAlpides(), geometry->GetNAlpidesActive());
}

