import serial
import argparse
import time
import re
from blessed import Terminal


devicemap = {"540303": "PSU 6", "495234": "PSU 1", "495228": "PSU 2", "540362": "PSU 3", "452600": "PSU 4","540299":"PSU 5"}
term = Terminal()

def printStatus(serial):
    try:
        serial.write(b'V1?\r\n')
        volt1 = serial.readline().decode(encoding="utf-8")[:-2]
        volt1 = float(re.findall("\d+\.\d+", volt1)[0])
    except:
        print(term.move_down() + term.move_x(1) ,'\033[91m'+"Could not read Voltage value"+ term.move_down() + term.move_x(1) +"Please try again"+ term.move_down() + term.move_x(1) +"Exiting..."+'\033[0m', end='')
        exit()

    try:
        serial.write(b'I1?\r\n')
        amps1 = serial.readline().decode(encoding="utf-8")[:-2]
        amps1 = float(re.findall("\d+\.\d+", amps1)[0])
    except:
        print(term.move_down() + term.move_x(1) ,'\033[91m'+"Could not read Amps value"+ term.move_down() + term.move_x(1) +"Please try again"+ term.move_down() + term.move_x(2) +"Exiting..."+'\033[0m', end='')
        exit()

    try:
        serial.write(b'OVP1?\r\n')
        ovp1 = serial.readline().decode(encoding="utf-8")[:-2]
        ovp1 = float(re.findall("\d+\.\d+", ovp1)[0])
    except:
        print(term.move_down() + term.move_x(1) ,'\033[91m'+"Could not read OVP value"+ term.move_down() + term.move_x(2) +"Does the PSU support it?"+ term.move_down() + term.move_x(2) +"Displaying OVP as 0.0"+'\033[0m', end='')
        ovp1 = 0.0
    
    try:
        serial.write(b'OCP1?\r\n')
        ocp1 = serial.readline().decode(encoding="utf-8")[:-2]
        ocp1 = float(re.findall("\d+\.\d+", ocp1)[0])
    except:
        print(term.move_down() + term.move_x(1) ,'\033[91m'+"Could not read OCP value"+ term.move_down() + term.move_x(2) +"Does the PSU support it?"+ term.move_down() + term.move_x(2) +"Displaying OCP as 0.0"+'\033[0m', end='')
        ocp1 = 0.0

    try:
        serial.write(b'I1O?\r\n')
        curAmps1 = serial.readline().decode(encoding="utf-8")[:-2]
        curAmps1 = float(re.findall("\d+\.\d+", curAmps1)[0])
    except:
        print(term.move_down() + term.move_x(1) ,'\033[91m'+"Could not read current Amps value"+ term.move_down() + term.move_x(2) +"Does the PSU support it?"+ term.move_down() + term.move_x(2) +"Displaying as 0.0"+'\033[0m', end='')
        curAmps1 = 0.0

    try:
        serial.write(b'OP1?\r\n')
        stat1 = serial.readline().decode(encoding="utf-8")[:-2]
        if stat1 == '1':
            stat1 = 'on'
        elif stat1 == '0':
            stat1 = 'off'
    except:
        print(term.move_down() + term.move_x(1) ,'\033[91m'+"Could not read output Status"+ term.move_down() + term.move_x(1) +"Please try again"+ term.move_down() + term.move_x(1) +"Exiting..."+'\033[0m', end='')
        exit()

    print(term.move_down() + term.move_x(1) ,"="*16, end='')
    print(term.move_down() + term.move_x(1) ,"Channel 1", end='')
    print(term.move_down() + term.move_x(1) ,"Voltage setpoint:\t",volt1, end='')
    print(term.move_down() + term.move_x(1) ,"Current limit:\t", amps1, end='')
    print(term.move_down() + term.move_x(1) ,"OVP trippoint:\t", ovp1, end='')
    print(term.move_down() + term.move_x(1) ,"OCP trippoint:\t", ocp1, end='')
    print(term.move_down() + term.move_x(1) ,"Output status:\t", stat1, end='')
    print(term.move_down() + term.move_x(1) ,"Output current:\t", curAmps1, end='')

    if volt1 > ovp1 and ovp1 > 0:
        print(term.move_down() + term.move_x(1) ,'\033[91m'+"WARNING:"+ term.move_down() +term.move_x(60) +"Voltage set higher than OVP!"+'\033[0m', end='')

    if amps1 > ocp1 and ocp1 > 0:
        print(term.move_down() + term.move_x(1) ,'\033[91m'+"WARNING:"+ term.move_down() +term.move_x(60) +"Current limit set higher than OCP!"+'\033[0m', end='')

    print(term.move_down() + term.move_x(1) ,"="*16, end='')

    try: 
        serial.write(b'V2?\r\n')
        volt2 = serial.readline().decode(encoding="utf-8")[:-2]
    except:
        print(term.move_down() + term.move_x(60)  ,'\033[91m'+"Could not read Amps value"+ term.move_down() +term.move_x(60) +"Please try again"+ term.move_down() +term.move_x(60) +"Exiting..."+'\033[0m', end='')
        exit()

    if 'V' not in volt2:
        print("")
        return
    volt2 = float(re.findall("\d+\.\d+", volt2)[0])
    print(term.move(1,60))

    try:
        serial.write(b'I2?\r\n')
        amps2 = serial.readline().decode(encoding="utf-8")[:-2]
        amps2 = float(re.findall("\d+\.\d+", amps2)[0])
    except:
        print(term.move_down() + term.move_x(60)  ,'\033[91m'+"Could not read Amps value"+ term.move_down() +term.move_x(60) +"Please try again"+ term.move_down() +term.move_x(60) +"Exiting..."+'\033[0m', end='')
        exit()

    try:
        serial.write(b'OVP2?\r\n')
        ovp2 = serial.readline().decode(encoding="utf-8")[:-2]
        ovp2 = float(re.findall("\d+\.\d+", ovp2)[0])
    except:
        print(term.move_down() + term.move_x(60)  ,'\033[91m'+"Could not read OVP value"+ term.move_down() +term.move_x(60) +"Does the PSU support it?"+ term.move_down() +term.move_x(60) +"Displaying OVP as 0.0"+'\033[0m', end='')
        ovp2 = 0.0
    
    try:
        serial.write(b'OCP2?\r\n')
        ocp2 = serial.readline().decode(encoding="utf-8")[:-2]
        ocp2 = float(re.findall("\d+\.\d+", ocp2)[0])
    except:
        print(term.move_down() + term.move_x(60)  ,'\033[91m'+"Could not read OCP value"+ term.move_down() +term.move_x(60) +"Does the PSU support it?"+ term.move_down() +term.move_x(60) +"Displaying OCP as 0.0"+'\033[0m', end='')
        ocp2 = 0.0
    
    try:
        serial.write(b'I2O?\r\n')
        curAmps2 = serial.readline().decode(encoding="utf-8")[:-2]
        curAmps2 = float(re.findall("\d+\.\d+", curAmps2)[0])
    except:
        print(term.move_down() + term.move_x(60) ,'\033[91m'+"Could not read current Amps value"+ term.move_down() + term.move_x(62) +"Does the PSU support it?"+ term.move_down() + term.move_x(62) +"Displaying as 0.0"+'\033[0m', end='')
        curAmps2 = 0.0

    try:
        serial.write(b'OP2?\r\n')
        stat2 = serial.readline().decode(encoding="utf-8")[:-2]
        if stat2 == '1':
            stat2 = 'on'
        elif stat2 == '0':
            stat2 = 'off'
    except:
        print(term.move_down() + term.move_x(60)  ,'\033[91m'+"Could not read output Status"+ term.move_down() +term.move_x(60) +"Please try again"+ term.move_down() +term.move_x(60) +"Exiting..."+'\033[0m', end='')
        exit()
    
    print(term.move_down() + term.move_x(60)  ,"="*16, end='')
    print(term.move_down() + term.move_x(60)  ,"Channel 2", end='')
    print(term.move_down() + term.move_x(60)  ,"Voltage setpoint:\t",volt2, end='')
    print(term.move_down() + term.move_x(60)  ,"Current limit:\t", amps2, end='')
    print(term.move_down() + term.move_x(60)  ,"OVP trippoint:\t", ovp2, end='')
    print(term.move_down() + term.move_x(60)  ,"OCP trippoint:\t", ocp2, end='')
    print(term.move_down() + term.move_x(60)  ,"Output status:\t", stat2, end='')
    print(term.move_down() + term.move_x(60) ,"Output current:\t", curAmps2, end='')

    if volt2 > ovp2 and ovp2 > 0:
        print(term.move_down() + term.move_x(60)  ,'\033[91m'+"WARNING:"+ term.move_down() +term.move_x(60) +"Voltage set higher than OVP!"+'\033[0m', end='')

    if amps2 > ocp2 and ocp2 > 0:
        print(term.move_down() + term.move_x(60)  ,'\033[91m'+"WARNING:"+ term.move_down() +term.move_x(60) +"Current limit set higher than OCP!"+'\033[0m', end='')
    print(term.move_down() + term.move_x(60)  ,"="*16)

def getPSUid(serial):
    try:
        serial.write(b'*IDN?\r\n')
        IDN = serial.readline().decode(encoding="utf-8")
        id = IDN.split(",")[2].strip()
        if id not in devicemap.keys():
            print("Unknown Device with ID: ", id)
        else:
            print(devicemap[id])
    except:
        print('\033[91m'+"Could not get PSU ID"+ term.move_down() +"Please try again!"+'\033[0m', end='')
        exit()

parser = argparse.ArgumentParser(description='Send commands to a powersupply')
parser.add_argument('--port', help='Serial Port to use', required=True)
parser.add_argument('--reboot',action='store_true', default=False,  help='Powercycle the output')
parser.add_argument('--on', action='store_true', default=False, help='Turn output on')
parser.add_argument('--off', action='store_true', default=False, help='Turn output off')
parser.add_argument('--untrip', action='store_true', default=False, help='Try to clear trip conditions')
parser.add_argument('--V', action='store', type=str, help='Change output Voltage')
parser.add_argument('--I', action='store', type=str, help='Change output Current limit')
parser.add_argument('--num', action='store', type=str, default=1, help='Chose output to change')
parser.add_argument('--OVP', action='store', type=str, help='Set over voltage trip point')
parser.add_argument('--OCP', action='store', type=str, help='Set over current trip point')

args = parser.parse_args()

print(term.home + term.clear, end='')
print(term.home())

try:
    ser = serial.Serial(args.port, 9600, rtscts=1, timeout=2)
except:
    print("Could not open serial connection!"+ term.move_down() +"Did you chose the right port?")
    exit()

getPSUid(ser)

if args.untrip:
    try:
        ser.write(b'OP1 0\r\n')
        time.sleep(1)
        ser.write(b'OP2 0\r\n')
        time.sleep(1)
        ser.write(b'TRIPRST\r\n')
        print("UNTRIP")
        exit()
    except Exception as e:
        print("Error: ",str(e))
        exit()

if args.V:
    try:
        com = 'V{} {}\r\n'.format(args.num, args.V)
        ser.write(bytes(com,encoding='utf-8'))
        print('Set Voltage to:',args.V)
    except Exception as e:
        print("Error: ",str(e))
        exit()

if args.I:
    try:
        com = 'I{} {}\r\n'.format(args.num, args.I)
        ser.write(bytes(com,encoding='utf-8'))
        print('Set current limit to:',args.I)
    except Exception as e:
        print("Error: ",str(e))
        exit()

if args.reboot:
    try:
        com = 'OP{} 0\r\n'.format(args.num)
        ser.write(bytes(com, encoding="utf-8"))
        time.sleep(5)
        com = 'OP{} 1\r\n'.format(args.num)
        ser.write(bytes(com, encoding="utf-8"))
        print("Powercycle done!")
    except Exception as e:
        print("Error: ",str(e))
        exit()

if args.off:
    try:
        com = 'OP{} 0\r\n'.format(args.num)
        ser.write(bytes(com, encoding="utf-8"))
        print("Output off!")
    except Exception as e:
        print("Error: ",str(e))
        exit()

if args.on:
    try:
        com = 'OP{} 1\r\n'.format(args.num)
        ser.write(bytes(com, encoding="utf-8"))
        print("Output on!")
    except Exception as e:
        print("Error: ",str(e))
        exit()


if args.OVP:
    try:
        com = 'OVP{} {}\r\n'.format(args.num, args.OVP)
        ser.write(bytes(com,encoding='utf-8'))
        print('Set overvoltage trip pont to:',args.OVP)
    except Exception as e:
        print("Error: ",str(e))
        exit()

if args.OCP:
    try:
        com = 'OCP{} {}\r\n'.format(args.num, args.OCP)
        ser.write(bytes(com,encoding='utf-8'))
        print('Set overcurrent trip pont to:',args.OCP)
    except Exception as e:
        print("Error: ",str(e))
        exit()
printStatus(ser)
