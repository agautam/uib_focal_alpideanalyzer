#include "Pixel.h"

struct by_x {
    bool operator()(Pixel const &a, Pixel const &b) const noexcept {
        return a.x < b.x;
    }
};

struct by_y {
    bool operator()(Pixel const &a, Pixel const &b) const noexcept {
        return a.y < b.y;
    }
};

