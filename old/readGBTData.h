#ifndef GBT
#define GBT

#include "TTree.h"
#include "TFile.h"

#include "TH2D.h"
#include <vector>

int counter = 0;

TTree *dataTree;
TTree *headerTree;
TFile *dataFile;

//vector<TH2D*> alpideMaps;
vector<int> alpideIDs;
//TH2D *newAlpideMap(int id);
//string filename = "data2pix.raw";

int alpideBX;
int alpideID;
int alpideX;
int alpideY;
ULong64_t triggerORBIT;
unsigned int triggerBX;
unsigned int triggerBX_delta;
ULong64_t triggerTime;
ULong64_t triggerTime_delta;
int eventID; // Viljar's eventID
unsigned int triggerID;

unsigned int feeIDLayer;
unsigned int feeIDGBTLink;
unsigned int feeIDStaveNumber;

unsigned long filePosition;

std::vector<int> activelanes;
#endif
