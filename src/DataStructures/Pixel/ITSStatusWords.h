#ifndef ITS_STATUSWORD
#define ITS_STATUSWORD

#define ITS_HEADERWORD_IDENTIFIER           (char) 0xe0
#define ITS_TRIGGERDATAHEADER_IDENTIFIER    (char) 0xe8
#define ITS_TRIGGERDATATRAILER_IDENTIFIER   (char) 0xf0
#define ITS_DIAGNOSTICSDATAWORD_IDENTIFIER  (char) 0xe4
#define ITS_CALIBRATIONDATAWORD_IDENTIFIER  (char) 0xf8

struct ITSHeaderWord {
    unsigned char activelanes[4];
    unsigned char reserved[5];
    unsigned char identifier; // SUM: 10 Bytes
};

struct ITSTriggerDataHeader {
    unsigned char triggertype[2];
    unsigned char triggerbc[2];
    unsigned char triggerorit[4];
    unsigned char reserved;
    unsigned char identifier; // SUM: 10 Bytes
};

struct ITSTriggerDataTrailer {
    unsigned char lanestatus[7];
    unsigned int timeout[2];
};

#endif
