#ifndef PROGRESSMONITOR
#define PROGRESSMONITOR

#include <chrono>
#include <iostream>
#include <iomanip>

/* very simple class which outputs
   while looping through large files */
class ProgressMonitor{
    const uint64_t nmax;
    const uint64_t rate;

    public:
        ProgressMonitor(unsigned long long _n, unsigned long long _r) ;
        ProgressMonitor() : nmax(1), rate(1){};
        void Update(uint64_t _pos);
        unsigned long long Update();
        void Output();
        void Output(uint64_t _pos);
        //void SetRate(unsigned long long);
        void SetPosition(unsigned long long );
        //void SetMax(unsigned long long);
        unsigned long long GetPosition();
        unsigned long long GetMax();
        unsigned long long GetRate();



    private:    
        std::chrono::time_point<std::chrono::steady_clock> start;
        std::chrono::time_point<std::chrono::steady_clock> stop;
        uint64_t position;

};



#endif // PROGRESSMONITOR
