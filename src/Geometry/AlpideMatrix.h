#ifndef ALPIDE_MATRIX
#define ALPIDE_MATRIX

class AlpideMatrix{
    
    public:
        static const short N_ALPIDE_COLS = 1024;
        static const short N_ALPIDE_ROWS = 512;
        AlpideMatrix();
};

#endif