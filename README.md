In analyzeALPIDE.C:
1.) update filename and path to your raw file
2.) run 'root analyzeALPIDE.C+'
3.) should produce a ROOT file in your home folder

In readALPIDETree.C:
1.) update filename and path to the root file produced by analyzeALPIDE.C
2.) run 'root readALPIDETree.C+'
3.) should produce a text file with the hits and the noisy pixels of the ROOT file in your home folder
