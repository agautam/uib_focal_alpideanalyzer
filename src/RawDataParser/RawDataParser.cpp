#include "RawDataParser.h"
#include "RawDataHeader.h" 
#include "ITSStatusWords.h"
#include "LaneData.h"
#include "ProgressMonitor.h"

#include "TTree.h"
#include "TFile.h"

#include <fstream>
#include <istream>
#include <string>
#include <cstring>
#include <cstdint>
#include <sstream>

void RawDataParser::init(TFile *file){
    
    writefile = file;
    writefile->cd();
    
    rdhtree = new TTree("RDH", Form("RDH of %s", filepath.c_str()));
    rdhtree->Branch("feeid", &feeid, "feeid/s");
    rdhtree->Branch("page", &page, "page/s");
    rdhtree->Branch("stopbit", &(rdh.stopbit), "stopbit/b");
    rdhtree->Branch("memorysize", &memsize, "memorysize/s");
    rdhtree->Branch("packetcounter", &(rdh.packetcounter), "packetcounter/b");
    rdhtree->Branch("orbit", &orbit, "orbit/i");
    rdhtree->Branch("bc", &bx, "bc/s");
    rdhtree->SetMaxTreeSize(maxtreesize);

    printf("RawDataParser ROOT file and tree initialized\n");
}

RawDataParser::RawDataParser(std::string _fp, std::string _cp){


    config = {};
    loadConfig(_cp);

    filepath = _fp;
    infile.open(_fp.c_str(), std::ios::in | std::ios::binary);

    std::streampos endoffile;

    if(!infile.good()){
        printf("Raw file %s not found. Cannot parse non-existing file.\n", _fp.c_str());
    } else {
        infile.seekg(0,infile.end);
        endoffile = infile.tellg();
        printf("Processing raw file %s of size %.2f GB...\n", _fp.c_str(), (double) ((double) endoffile / 0x40000000) );
        infile.seekg(0, infile.beg);
    }
    printf("%ld\n", sizeof(rdh));

    for(unsigned int l=0;l<N_LANES;l++){
        allLanes[l].size=0;
    }

    progressmonitor = new ProgressMonitor((unsigned long long) endoffile, (unsigned long long) PM_OUTPUT_RATE);

    feeid = 0;
    page = 0;
    memsize = 0;
    orbit = 0;
    bx = 0;

    rdh = {};

    detectordata = new char[LENGTH_DATA];    
    identifier = new char[LENGTH_IDENTIFIER];    
    padding = new char[LENGTH_PADDING];    

    printf("RawDataParser created\n");
}

bool RawDataParser::readRDH(){
    if(!infile.good()) return false;
    infile.read((char*) &rdh, RDH_LENGTH);
    for(int d=-3;d<=0;d++){
    	progressmonitor->Update((long long int) (((std::streamoff)infile.tellg())-infile.beg)+d);
    	progressmonitor->Output();
    }
    if(infile.good()){
        feeid = getRDHElement(rdh, RDH_FEEID);
	orbit = getRDHElement(rdh, RDH_ORBIT);
	memsize = getRDHElement(rdh, RDH_MEMORY_SIZE);
	page = getRDHElement(rdh, RDH_PAGES_COUNTER);
    } 
    if(config.printRDH){
        printRDH(rdh, config.printRDHDetails);
    }
    writefile->cd();
    rdhtree->Fill();
    if(infile.good()){
        return true;
    } else {
        return false;
    }
}

void RawDataParser::resetLanes(){
    for(unsigned int l=0;l<N_LANES;l++){
        allLanes[l].size = 0;
        allLanes[l].lane = l;
        allTriggers[l].size = 0;
    }
}

void RawDataParser::printLaneSummary(){
    if(config.printLaneSummary){
        printf("Lane summary:\n");
        for(unsigned int l=0;l<N_LANES;l++){
            if(allLanes[l].size != 0){
                printf("\tLane %d: %d B of data\n", l, allLanes[l].size);
            }
        } 
        printf("\n");
    }
}

bool RawDataParser::readDetectorData(){
   
    if(!infile.good()) {
	if(!exit) std::cout << "ERROR: Input file is not good. Exiting." << std::endl;
	exit = true;
	return false;
    }
    //std::cout << "Position: " << infile.tellg() << " (" << nextpacket<< ")"; 

    if(((long long int) infile.tellg()) >= nextpacket) {
    	//std::cout << "... exiting" << std::endl;
        return false;
    } 

    if(!infile.read( detectordata , LENGTH_DATA ).good()){
	    printf("ERROR: reading detector data failed\n");
    }
    if(!infile.read( identifier, LENGTH_IDENTIFIER ).good()){
	    printf("ERROR: reading identifier failed\n");
    }
    if(!infile.read( padding , LENGTH_PADDING ).good()){
	    printf("ERROR: reading padding failed\n");
    }

    progressmonitor->Update((long long int) (((std::streamoff)infile.tellg())-infile.beg));
    progressmonitor->Output();

    if(config.noDetectorData){
        return true;
    }

    if(identifier[0] == ITS_HEADERWORD_IDENTIFIER) {
        
        if(config.printDetectorData){
            printf("ITS header word - active lanes: ");
        }

        for(unsigned int i=0;i<=N_LANES;i+=8){
            //printf("0x%02x ", detectordata[i/8]);
            for(int l=0;l<8;l++){
                if(detectordata[i/8] & (1<<l)){ 
                    int lane = N_LANES-i-(8-l);
                    if(config.printDetectorData){
                        printf("%d ",lane);
                    }
                }
            }
        }
        if(config.printDetectorData){
            printf("\n");
        }
    }
    else if(identifier[0] == ITS_TRIGGERDATAHEADER_IDENTIFIER) {
        //printf("ITS trigger data header\n");
        bx =  ((unsigned char) detectordata[2]) + ((unsigned int) ( detectordata[3] << 8));
        orbit =  (uint64_t) ( 
            ((uint64_t) ( ((unsigned char)detectordata[4]) << 0)) + 
            ((uint64_t) ( ((unsigned char)detectordata[5]) << 8)) + 
            ((uint64_t) ( ((unsigned char)detectordata[6]) << 16)) + 
            ((uint64_t) ( ((unsigned char)detectordata[7]) << 24)) 
	    );
        trigger = (uint64_t) ( ( orbit*3564 ) + bx); 
        if(config.printDetectorData){
            printf("ITS trigger data header (t = 0x%08lx bx)\n", trigger);
        }
        for(unsigned int l=0;l<N_LANES;l++){
            int s = allTriggers[l].size;
            int p = allLanes[l].size;
            allTriggers[l].trigger[s] = trigger;
            allTriggers[l].position[s] = p;
            allTriggers[l].size++;
        }
	//for(int i=0;i<LENGTH_DATA;i++){
	//    printf("%02x ", (unsigned char) detectordata[i]);
	//}
	//printf(" -- ");
	//for(int i=4;i<LENGTH_DATA;i++){
	//    printf("%lu ", (uint64_t) ((uint64_t) ((unsigned char)detectordata[i])<<((i-4)*8) ));
	//}
	//printf("%lu %lu \n",trigger, orbit);
	

    }
    else if(identifier[0] == ITS_DIAGNOSTICSDATAWORD_IDENTIFIER) {
        //printf("ITS DDW\n");
        
    }
    else if(identifier[0]== ITS_CALIBRATIONDATAWORD_IDENTIFIER){

    }
    else if(identifier[0] == ITS_TRIGGERDATATRAILER_IDENTIFIER) {
        if(config.printDetectorData){
            printf("ITS trigger data trailer\n");
        }
    }
    else{

        int lane = (identifier[0]/0x10)*10 + (identifier[0]% 0x10) -1;
        //printf("%d\n", lane);

        int currpos = allLanes[lane].size;
        memcpy( &allLanes[lane].laneData[currpos], &detectordata[0], LENGTH_DATA);
        allLanes[lane].size+=LENGTH_DATA;
        allLanes[lane].lane = lane;

#ifdef DATA_OUTPUT
        printf("%d (%d): ", currpos, lane);
        for(unsigned int i=0;i<LENGTH_DATA;i++){
            printf("%02x ", (unsigned char) detectordata[i]);
        }
        printf("\n");
#endif //DATA_OUTPUT

    }

    return true;
}

//std::streampos RawDataParser::step(int stepsize){
//    std::streampos position = infile.tellg();
//    position += (unsigned long long) (stepsize - RDH_LENGTH);
//    infile.seekg(position);
//    return position;
//}

bool RawDataParser::good(){
    if(mode==FINISHED) return false;
    if(exit) return exit;
    return !infile.eof();
}

int RawDataParser::loadConfig(std::string configfilepath){
    std::ifstream configfile(configfilepath);
    if(!configfile.good()){
        std::cout << "ERROR: parser config file " << configfilepath << " not found. Using default configuration." << std::endl;
        return false;
    }
    std::string line;
    while(std::getline(configfile, line)){
        
        if(line.substr(0)=="#") continue;
        if(line.length()==0) continue;

        std::istringstream sin = std::istringstream(line.substr(line.find("=")+1));
    
        if(line.find("printRDHHeader")!=std::string::npos){
            sin >> config.printRDH;
            continue;
        }
        if(line.find("printRDHDetails")!=std::string::npos){
            sin >> config.printRDHDetails;
            continue;
        }
        if(line.find("printDetectorData")!=std::string::npos){
            sin >> config.printDetectorData;
            continue;
        }
        if(line.find("printLaneSummary")!=std::string::npos){
            sin >> config.printLaneSummary;
            continue;
        }
        if(line.find("outputDirectory")!=std::string::npos){
            sin >> config.outputDirectory;
	    std::string mkdir = "mkdir -p " + config.outputDirectory; 
	    system(mkdir.c_str());
	    printf("Created output directory %s (if not yet existed)\n", config.outputDirectory.c_str());
            continue;
        }
        if(line.find("ddw")!=std::string::npos){
            sin >> config.ddw;
            continue;
        }
        if(line.find("NumberOfRDHPackets")!=std::string::npos){
            sin >> config.nRDH;
            continue;
        }
        if(line.find("noDetectorData")!=std::string::npos){
            sin >> config.noDetectorData;
            continue;
        }
        if(line.find("useGlobalGeometry")!=std::string::npos){
            sin >> config.useGlobalGeometry;
            continue;
        }
	

    }

    configfile.close();
    return true;
}

void RawDataParser::WriteToFile(){
  writefile->cd();
  TList *list = writefile->GetList();
  TIter next(list);
  //list->ls();
  TObject *obj;
  writefile->cd();
  while((obj = (TObject*)next())){
    if(obj->InheritsFrom("TNamed")) {
      printf("Writing %s %s to file %s\n", obj->ClassName(), obj->GetName(),  writefile->GetName());
      obj->Write();
    }
  }


}

void RawDataParser::SetWriteFile(TFile *f){
	writefile = f;
}
void RawDataParser::WriteToFile(TFile *f){
	writefile = f;
	WriteToFile();
}
