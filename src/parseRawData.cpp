#include "RawDataParser.h"
#include "RawDataHeader.h"
#include "ITSStatusWords.h"
#include "AlpideDecoder.h"
#include "AlpideLayerGeometry.h"
#include "LaneData.h"

#include "TFile.h"


int main(int argc, char* argv[]){


    std::string sourcepath;
    std::string outfilename;
    std::string configfilepath = "/home/max/uib_focal_alpideanalyzer/config/ParserConfig.txt";

    if(argc==1){
        printf("\nNo file provided. Exiting.\n");
        return 0;
    } else if(argc==2){
        sourcepath = argv[1];
        outfilename = sourcepath.substr(sourcepath.find_last_of("/")+1);
        outfilename = outfilename.substr(0, outfilename.find_last_of("."));
        outfilename += ".root";
        std::cout << outfilename << std::endl;
    }
    

    //std::string sourcepath = "/home/mxr/data/focal/data.raw";
    RawDataParser rdp = RawDataParser(sourcepath, configfilepath);
    //rdp.loadConfig("../config/ParserConfig.txt");
    rdp.onlyRDH = true;
    rdp.mode = NEW_PACKET;
    rdp.nextpacket = 0;

    AlpideDecoder alpidedecoder = AlpideDecoder();

    TFile *alpidetreefile = new TFile(Form("%s/%s", rdp.config.outputDirectory.c_str(), ("hits_"+outfilename).c_str()), "RECREATE");
    TFile *alpidehistfile = new TFile(Form("%s/%s", rdp.config.outputDirectory.c_str(), ("maps_"+outfilename).c_str()), "RECREATE");
    TFile *rdhtreefile = new TFile(Form("%s/%s", rdp.config.outputDirectory.c_str(), ("rdh_"+outfilename).c_str()), "RECREATE");
   

    alpidedecoder.initFiles(alpidetreefile, alpidehistfile);
   
    AlpideLayerGeometry *layergeometry; 
    layergeometry = new AlpideLayerGeometry();
    if(rdp.config.useGlobalGeometry){
    	layergeometry->SetSPSBeamTest2021Geometry();
    	alpidedecoder.SetAlpideLayerGeometry(layergeometry);
    }
    
    
    rdp.init(rdhtreefile);
    //alpidedecoder.SetWriteFile(alpidetreefile);
    //rdp.SetWriteFile(rdhtreefile);


    int nRDH = 0;
    int nRDHPages = 0;
    int nRDHCompleted = 0;

    rdp.resetLanes();

    while(rdp.good()){

        if( (rdp.config.nRDH>=0) && (nRDHCompleted>=rdp.config.nRDH)) break;

	if(rdp.exit) break;

        switch(rdp.mode){

            // expecting to read a completely new packet
            // i.e. start of new file or after stop bit of last packet
            case(NEW_PACKET):
                // reset data of all lanes;
                rdp.resetLanes();
                // read the the RDH
                rdp.readRDH();
                if(!rdp.good()) {
                    rdp.mode = FINISHED; // set to finished if rdp is not good
                    break;
                }
                
                // count number of rdh
                nRDH++;
                nRDHPages++;

                // set raw data parser to detector data mode
                rdp.mode = DETECTOR_DATA;
                // set data size of expected detector data
                rdp.packetsize = getRDHElement(rdp.rdh, RDH_MEMORY_SIZE);
                // set start position of next rdh packet start
                // i.e. start of next rdh
                rdp.nextpacket = rdp.nextpacket + rdp.packetsize;

                break;

            // expecting to read a rdh of a new page
            // this can also be the rdh with the stop bit
            case(NEW_PAGE):
                // read RDH
                rdp.readRDH();
                if(!rdp.good()) {
                    rdp.mode = FINISHED; // set to finished if rdp is not good
                    break;
                }

                nRDHPages++;

                // check if RDH stop bit is set
                // i.e. if EOP (End of Package)
                if(!rdp.rdh.stopbit){
                    rdp.mode = DETECTOR_DATA;
                    rdp.packetsize = getRDHElement(rdp.rdh, RDH_MEMORY_SIZE);
                    rdp.nextpacket = rdp.nextpacket + rdp.packetsize;
                } else if(rdp.rdh.stopbit){
                    nRDHCompleted++;
                    rdp.nextpacket += getRDHElement(rdp.rdh, RDH_MEMORY_SIZE);
		            if(rdp.config.ddw){
		                rdp.readDetectorData();
		            }
                    rdp.mode = NEW_PACKET;

                    // do necessary detector analysis actions here
                    rdp.printLaneSummary();
                    
		            if(rdp.config.noDetectorData) break;
		            for(unsigned l=0;l<N_LANES;l++){
                        if(rdp.allLanes[l].size>0){
                            alpidedecoder.SetFeeId(rdp.feeid);
                            alpidedecoder.DecodeEvent(rdp.allLanes[l], rdp.allTriggers[l]);
                        }
                    }


                    
                }
                break;

            // loop thorugh detector data
            case(DETECTOR_DATA):
                while(rdp.readDetectorData()){     
                    // loop through the detector data
                    // detector data are filled in the raw data parser   
                }
                rdp.mode = NEW_PAGE;
                break;

	    case(FINISHED):
            default:
                break;

            if( (rdp.config.nRDH>=0) && (nRDHCompleted>=rdp.config.nRDH)) break;

        }
        
    }

    printf("%d RDH (%d stop bits) found (%d RDH pages)\n", nRDH, nRDHCompleted, nRDHPages);

    //alpidedecoder.pixelhittree->Write();
    alpidedecoder.WriteToFile();
    rdp.WriteToFile();
    alpidetreefile->Close();
    rdhtreefile->Close();

    return 0;
}
