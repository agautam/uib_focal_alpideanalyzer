#ifndef ALPIDE_LAYER_GEOMETRY
#define ALPIDE_LAYER_GEOMETRY

#include <string>
#include <map>
#include "AlpideCoordinateSystem3D.h"

typedef std::pair<unsigned int, unsigned int> AlpidePosition;


class AlpideLayerGeometry{

    private:
        std::map<AlpidePosition, AlpideCoordinateSystem3D *> alpides;
        unsigned int nAlpides = 0;
        unsigned int nAlpidesActive = 0;

	std::string name;

        double xmax, xmin, ymax, ymin;

    public:
        AlpideCoordinateSystem3D *GetAlpideCoordinates(unsigned short _fee, unsigned short _id);
        void SetSPSBeamTest2021Geometry();
        unsigned int GetNAlpides();
        unsigned int GetNAlpidesActive();

        double GetXmax();
        double GetXmin();
        double GetYmax();
        double GetYmin();

	std::string GetName();

};

#endif
