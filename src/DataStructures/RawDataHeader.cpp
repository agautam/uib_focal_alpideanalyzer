#include "RawDataHeader.h"
#include <iostream>
#include <cstdint>
#include <cstdio>


unsigned int getRDHElement(RawDataHeader &rdh, RDHElement rdhe){
    
    switch(rdhe){
        case RDH_MEMORY_SIZE:
            return ( (rdh.memorysize[1] <<8) + (rdh.memorysize[0]) ); 
        case RDH_FEEID:
            return( /*(rdh.feeid[1] <<8) + */(rdh.feeid[0]) ); 
        case RDH_ORBIT:
            uint32_t orbit;
            orbit = 0;
            for(unsigned int i=0;i<4;i++){
                orbit+= (uint32_t) (rdh.orbit[i]<<(8*i));
                //triggerORBIT =  (ULong64_t)  ( /*((ULong64_t) rawdata[3] )*/ + ((ULong64_t) rawdata[4] << 0) + ((ULong64_t) rawdata[5] << 8) + ((ULong64_t) rawdata[6] << 16) + ((ULong64_t) rawdata[7] << 24) );
            }
            return orbit; 
        case RDH_STOP_BIT:
            return rdh.stopbit;
        case RDH_PAGES_COUNTER:
            return( (rdh.pagescounter[1] <<8) + (rdh.pagescounter[0]) ); 
         
        default:
            return 0;
    }

}

void printRDH(RawDataHeader &rdh, bool details){
    printf("Raw Data Header (RDH, page %d, stop bit %d)\n", getRDHElement(rdh, RDH_PAGES_COUNTER), (unsigned int) rdh.stopbit);

    if(details){
        printf("-------------------------------------------\n");
        printf("Version:\t%d\n",                (unsigned int) rdh.version);
        printf("Size:\t\t%d\n",                 (unsigned int) rdh.size);
        printf("Fee ID:\t\t%d\n",               getRDHElement(rdh, RDH_FEEID));
        printf("Source ID:\t%d\n",              (unsigned int) rdh.sourceid);
        printf("Memory size:\t0x%04x (%d) \n",  getRDHElement(rdh, RDH_MEMORY_SIZE), getRDHElement(rdh, RDH_MEMORY_SIZE));
        printf("Link ID:\t%d\n",                (unsigned int) rdh.linkid);
        printf("Packet counter:\t%d\n",         (unsigned int) rdh.packetcounter);
        printf("Orbit:\t\t0x%04x (%d BX)\n",            getRDHElement(rdh, RDH_ORBIT), 3564*getRDHElement(rdh, RDH_ORBIT));
        printf("BC:\t\t%04x\n",                 getRDHElement(rdh, RDH_BC));
        printf("Page:\t\t%d\n",                 getRDHElement(rdh, RDH_PAGES_COUNTER));
        printf("Stop bit:\t%d\n",               (unsigned int) rdh.stopbit);
        printf("-------------------------------------------\n");
        printf("\n");
    }
    if(rdh.stopbit) printf("\n");

}
