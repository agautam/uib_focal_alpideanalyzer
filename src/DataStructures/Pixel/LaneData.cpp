#include "LaneData.h"

#include <cstdint>

uint64_t TriggerForByte(TriggerData td, int pos){

    for(int i=0;i<MAX_TRIGGERS_PER_PACKET;i++){
        if(td.position[i]>=pos){
            return td.trigger[i];
        }
    }
    return 0;
}
