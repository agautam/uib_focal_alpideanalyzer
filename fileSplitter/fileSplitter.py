#!/bin/python
#Python script for splitting large files into smaller files
import argparse
import time
import os

DATA_LENGTH = 10
ZERO_LENGTH = 6

def splitFile(filename, size=50):
    start = time.time()
    total_bytes = 0
    with open(filename, "rb") as fin:
        bytes = fin.read((DATA_LENGTH+ZERO_LENGTH)*1000000)
        filename = filename.split(".")
        total_bytes += 16000000
        num_of_files = 0
        os.mkdir(filename[0])
        fout = open(filename[0] + '/' + filename[0]+"_%d."%num_of_files + filename[1], "wb")
        print("Writing to file: " + filename[0]+"_%d."%num_of_files + filename[1])
        while bytes != b"":
            if (total_bytes >= size*10**9):
                if (bytes[0] == 6 and bytes[1] == 64):
                    num_of_files += 1
                    total_bytes = 0
                    fout.close()
                    fout = open(filename[0] + '/' + filename[0]+"_%d."%num_of_files + filename[1], "wb")
                    print("Writing to file: " + filename[0]+f"_{num_of_files}." + filename[1])
                fout.write(bytes)
                bytes = fin.read(DATA_LENGTH+ZERO_LENGTH)
                total_bytes += 16
            else:
                fout.write(bytes)
                bytes = fin.read((DATA_LENGTH+ZERO_LENGTH)*1000000)
                total_bytes += 16000000
            percentage = total_bytes/(size*10**9)*100
            print(str(round(percentage))+"%% file complete", end="\r", flush=True)
        fout.close()
    end = time.time()
    total_time = end - start
    print("Total time used: " + str(total_time))

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Split the file into multiple smaller files at given size.")
    parser.add_argument("--source", type=str, required=True, help="Relative path to file to split.")
    parser.add_argument("--GB", type=float, required=False, help="How large each file should be in GB (except the last file). Default value is 50 GB.")
    args = parser.parse_args()

    splitFile(args.source, args.GB)
