#ifndef READALPIDETREE
#define READALPIDETREE

struct noisyPixel {
    int id;
    int x;
    int y;
    long int n;
};

struct by_hits {
    bool operator()(noisyPixel const &a, noisyPixel const &b) const noexcept {
        return a.n > b.n;
    }
};
struct by_id {
    bool operator()(noisyPixel const &a, noisyPixel const &b) const noexcept {
        return a.id < b.id;
    }
};
struct by_x {
    bool operator()(noisyPixel const &a, noisyPixel const &b) const noexcept {
        return a.x < b.x;
    }
};
struct by_y {
    bool operator()(noisyPixel const &a, noisyPixel const &b) const noexcept {
        return a.y < b.y;
    }
};


#endif
