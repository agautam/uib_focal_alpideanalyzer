# Powersupply control

## Usage
```
usage: pscontrol.py [-h] [--port PORT] [--reboot] [--on] [--off] [--untrip] [--V V] [--I I] [--num NUM] [--OVP OVP] [--OCP OCP]

Send commands to a powersupply

optional arguments:
  -h, --help   show this help message and exit
  --port PORT  Serial Port to use
  --reboot     Powercycle the output
  --on         Turn output on
  --off        Turn output off
  --untrip     Try to clear trip conditions
  --V V        Change output Voltage
  --I I        Change output Current limit
  --num NUM    Chose output to change
  --OVP OVP    Set over voltage trip point
  --OCP OCP    Set over current trip point
```

## Examples
```
python pscontrol.py --port /dev/cu.usbmodem4526001 --V 5 --I 1 --num 2 --OCP 1.1 --OVP 11 --off
```
Sets output voltage of channel 2 to 5V current limit to 1A, OCP to 1.1A, OVP to 11V and turns off the output

## Requirements

- pyserial
- blessed

