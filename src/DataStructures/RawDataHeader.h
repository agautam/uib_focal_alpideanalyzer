#ifndef RAWDATAHEADER
#define RAWDATAHEADER

#define RDH_LENGTH 64

//#define PRINT_RDH_DETAILS

enum RDHElement {
    RDH_VERSION,
    RDH_SIZE,
    RDH_FEEID,
    RDH_PRIORITY_BIT,
    RDH_SOURCE_ID,
    RDH_OFFSET_NEW_PACKET,
    RDH_MEMORY_SIZE,
    RDH_LINK_ID,
    RDH_PACKET_COUNTER,
    RDH_CRU_ID,
    RDH_DW,
    RDH_BC,
    RDH_ORBIT,
    RDH_TRG_TYPE,
    RDH_PAGES_COUNTER,
    RDH_STOP_BIT,
    RDH_DETECTOR_FIELD,
    RDH_PAR_BIT
};

struct RawDataHeader{
    unsigned char version;              
    unsigned char size;                 
    unsigned char feeid[2];              // -- SUM: 4Bytes
    unsigned char prioritybit;          //
    unsigned char sourceid;             //
    unsigned char reserved1[2];         // -- SUM: 8 Bytes
    unsigned char offsetnewpacket[2];   //
    unsigned char memorysize[2];        // -- SUM: 12 Bytes
    unsigned char linkid;               // 
    unsigned char packetcounter;        //
    unsigned char cruid_dw[2];          // -- SUM: 16 Bytes
    unsigned char bc[2];                // 
    unsigned char reserved2[2];         // -- SUM: 20 Bytes
    unsigned char orbit[4];             //
    unsigned char reserved3[8];         // -- SUM: 32 Bytes
    unsigned char trgtype[4];           // -- SUM: 36 Bytes
    unsigned char pagescounter[2];      // -- SUM: 38 Bytes
    unsigned char stopbit;              // -- SUM: 39 Bytes
    unsigned char reserved4[9];         // -- SUM: 48 Bytes
    unsigned char detectorfield[4];     // -- SUM: 52 Bytes
    unsigned char parbit[2];            // -- SUM: 54 Bytes
    unsigned char reserved[10];         // -- SUM: 64 Bytes
};

unsigned int getRDHElement(RawDataHeader &rdh, RDHElement rdhe);
void printRDH(RawDataHeader &rdh, bool details);

#endif
