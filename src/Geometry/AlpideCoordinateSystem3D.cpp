#include "AlpideCoordinateSystem3D.h"

AlpideCoordinateSystem3D::AlpideCoordinateSystem3D(unsigned int _l, int _offx, int _offy, bool _invx, bool _invy)
 : AlpideCoordinateSystem2D(_offx, _offy, _invx, _invy){
    layer = _l;

}


AlpideCoordinateSystem3D::AlpideCoordinateSystem3D(){
    
}

unsigned int AlpideCoordinateSystem3D::GetLayer(){
    return layer;
}